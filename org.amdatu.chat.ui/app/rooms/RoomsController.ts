// <reference path="typeScriptDefenitions/libs.d.ts" />

import RoomsService = require('RoomsService')

class RoomsController {
    static $inject = ['RoomsService', '$location'];

    newRoomName : string;
    rooms : Room[];
    message : string;

    constructor(private roomsService : RoomsService, private $location : ng.ILocationService) {
        this.roomsService.getRooms().subscribe((rooms) => {
            this.rooms = rooms;
        });
    }

    createRoom() {
        console.log('creating ', this.newRoomName);
    }

    enterRoom(roomName:string) {
        this.$location.path('rooms/' + roomName);
    }


}

export = RoomsController