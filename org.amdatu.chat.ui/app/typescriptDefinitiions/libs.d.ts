/// <reference path="./requirejs/require.d.ts" />
/// <reference path="./angularjs/angular.d.ts" />
/// <reference path="./angularjs/angular-route.d.ts" />
/// <reference path="./rx/rx.d.ts" />
/// <reference path="./sockjs/sockjs.d.ts" />
/// <reference path="../rooms/Room.d.ts" />
/// <reference path="../rooms/Message.d.ts" />


declare module "require" {
    export = require;
}

declare module "angular" {
    export = angular;
}

declare module "SockJS" {
    export = SockJS
}

declare module "Stomp" {
    export = Stomp
}

declare module "Rx" {
    export = Rx
}
