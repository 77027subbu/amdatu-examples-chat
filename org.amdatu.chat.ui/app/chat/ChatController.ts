/**
 * Created by paul on 06/09/14.
 */
// <reference path="typeScriptDefenitions/libs.d.ts" />

import RoomsService = require('rooms/RoomsService')

class ChatController {
    static $inject = ['RoomsService', '$routeParams'];

    room : string;
    message : string;
    messages : Message[] = [];
    username : string;
    loggedIn : boolean = false;

    constructor(private roomsService : RoomsService, private $routeParams: ng.route.IRouteParamsService) {
        this.room = $routeParams['room'];
    }

    send() {
        this.roomsService.sendToRoom(this.room, {from: this.username, body: this.message});
        this.message = "";
    }

    login() {
        this.loggedIn = true;

        this.roomsService.enterRoom(this.room).subscribe((msg) => {
            this.messages.push(msg);
        });
    }
}

export = ChatController