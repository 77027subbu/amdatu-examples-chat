define(["require", "exports", 'angular', 'rooms/RoomsController', 'chat/ChatController', 'rooms/RoomsService', "angular-route"], function(require, exports, angular, RoomsController, ChatController, RoomsService) {
    var ngModule = angular.module('amdatu.chat.app', ['ngRoute']).config([
        '$routeProvider',
        function ($routeProvider) {
            $routeProvider.when('/rooms', {
                controller: 'RoomsController',
                controllerAs: 'rooms',
                templateUrl: 'rooms/views/rooms.html'
            }).when('/rooms/:room', {
                controller: 'ChatController',
                controllerAs: 'chat',
                templateUrl: 'chat/views/chat.html'
            }).otherwise({ redirectTo: '/rooms' });
        }
    ]);

    ngModule.controller('RoomsController', RoomsController);
    ngModule.controller('ChatController', ChatController);
    ngModule.service('RoomsService', RoomsService);
    ngModule.constant('BASE_URL', 'http://localhost:8080');

    
    return ngModule;
});
