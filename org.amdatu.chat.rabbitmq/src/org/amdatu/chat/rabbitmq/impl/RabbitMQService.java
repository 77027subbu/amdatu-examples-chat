package org.amdatu.chat.rabbitmq.impl;

import java.io.IOException;

import org.amdatu.chat.chatlog.ChatLogService;
import org.amdatu.chat.chatlog.Message;
import org.amdatu.chat.rabbitmq.MessageSender;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.apache.felix.dm.annotation.api.Stop;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

@Component
public class RabbitMQService implements MessageSender{
	private volatile Connection m_connection;
	private volatile Channel m_channel;
	
	@ServiceDependency
	private volatile ChatLogService m_chatLog;
	
	@Start
	public void start() throws IOException {
		ConnectionFactory factory = new ConnectionFactory();
		m_connection = factory.newConnection();
		m_channel = m_connection.createChannel();
		m_channel.basicConsume("chatbackend", true, "backend", new DefaultConsumer(m_channel) {

			@Override
			public void handleDelivery(String consumerTag, Envelope envelope,
					BasicProperties properties, byte[] body) throws IOException {
				
				String room = envelope.getRoutingKey();
				
				ObjectMapper mapper = new ObjectMapper();
				Message message = mapper.readValue(body, Message.class);
				
				if(!"System".equals(message.getFrom())) {
					message.setRoom(room);
					m_chatLog.save(message);
				}
			}
			
		});
	}
	
	@Override
	public void send(Message message) {
		try {
			
			ObjectMapper mapper = new ObjectMapper();
			
			m_channel.basicPublish("amq.topic", message.getRoom(), null, mapper.writeValueAsBytes(message));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Stop
	public void stop() throws IOException {
		m_channel.close();
		m_connection.close();
	}
}
