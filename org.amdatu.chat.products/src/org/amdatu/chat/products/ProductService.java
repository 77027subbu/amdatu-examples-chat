package org.amdatu.chat.products;

import java.util.List;

public interface ProductService {
	List<Product> listProductsByTag(String tag);
}
