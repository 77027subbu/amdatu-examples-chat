package org.amdatu.chat.adgenerator;

import java.util.List;
import java.util.stream.Collectors;

import org.amdatu.chat.chatlog.ChatLogService;
import org.amdatu.chat.chatlog.Message;
import org.amdatu.chat.products.Product;
import org.amdatu.chat.products.ProductService;
import org.amdatu.chat.rabbitmq.MessageSender;
import org.amdatu.chat.rooms.storage.Room;
import org.amdatu.chat.rooms.storage.RoomsStorageService;
import org.amdatu.chat.wordcounter.WordCounter;
import org.amdatu.scheduling.annotations.RepeatForever;
import org.amdatu.scheduling.annotations.RepeatInterval;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@Component
@RepeatInterval(period = RepeatInterval.SECOND, value = 5)
@RepeatForever
public class AdGenerator implements Job {

	@ServiceDependency
	private volatile WordCounter m_wordCounter;

	@ServiceDependency
	private volatile ChatLogService m_chatLogService;

	@ServiceDependency
	private volatile RoomsStorageService m_roomStrorage;
	
	@ServiceDependency
	private volatile MessageSender m_sender;
	
	@ServiceDependency
	private volatile ProductService m_productService;

	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		m_roomStrorage.list()
				.forEach(
						r -> {
							List<String> messages = m_chatLogService
									.getLastMessages(r.getName(), 5).stream()
									.map(Message::getBody)
									.collect(Collectors.toList());
							
							String mostPopular = m_wordCounter.getMostPopular(messages);
							System.out.println(mostPopular);
							m_productService.listProductsByTag(mostPopular).stream()
								.map(p -> mapProductToMessage(r,p))
								.forEach(m_sender::send);
						});
	}


	private Message mapProductToMessage(Room r, Product p) {
		Message message = new Message();
		message.setRoom(r.getName());
		message.setFrom("System");
		message.setBody(p.getName());
		message.setImageUrl(p.getImageUrl());
		
		return message;
	}
	
	

}
