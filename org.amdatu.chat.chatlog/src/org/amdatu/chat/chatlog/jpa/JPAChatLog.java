package org.amdatu.chat.chatlog.jpa;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.amdatu.chat.chatlog.ChatLogService;
import org.amdatu.chat.chatlog.Message;
import org.amdatu.jta.ManagedTransactional;
import org.amdatu.jta.Transactional;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component(provides=ManagedTransactional.class)
@Transactional
public class JPAChatLog implements ChatLogService, ManagedTransactional{
	
	@ServiceDependency
	private volatile EntityManager em;
	
	@Override
	public void save(Message message) {
		JPAMessage jpaMessage = new JPAMessage();
		jpaMessage.setRoom(message.getRoom());
		jpaMessage.setBody(message.getBody());
		jpaMessage.setTs(System.currentTimeMillis());
		
		em.persist(jpaMessage);
	}

	@Override
	public List<Message> getLastMessages(String room, int nrOfMessages) {
		TypedQuery<JPAMessage> query = em.createQuery("select m from JPAMessage m ORDER BY m.ts DESC", JPAMessage.class);
		query.setMaxResults(nrOfMessages);
		
		return query.getResultList().stream().map(JPAMessage::toMessage).collect(Collectors.toList());
	}
	
	@Override
	public Class<?> getManagedInterface() {
		return ChatLogService.class;
	}

}
